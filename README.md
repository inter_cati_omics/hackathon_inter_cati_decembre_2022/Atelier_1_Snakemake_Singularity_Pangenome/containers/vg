# vg Singularity container
### Package vg Version 1.44.0
VG:
For mapping PKGDESCRIPTION genotyping using Giraffe (Siren et al. Science 10.1126/science.abg8871).
Variation graph data structures, interchange formats, alignment, genotyping, and variant calling methods

Homepage:

https://github.com/vgteam/vg

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
vg Version: 1.44.0<br>
Singularity container based on the recipe: Singularity.vg_v1.44.0.def

Local build:
```
sudo singularity build vg_v1.44.0.sif Singularity.vg_v1.44.0.def
```

Get image help:
```
singularity run-help vg_v1.44.0.sif
```

Default runscript: vg<br>
Usage:
```
./vg_v1.44.0.sif --help
```
or:
```
singularity exec vg_v1.44.0.sif vg --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull vg_v1.44.0.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/vg/vg:latest

```

